package neo.training.designpattern.adapter;

import neo.training.designpattern.Client;

public class Adaptee {

  public void foo() {
    Client.echo("Adaptee foo");
  }

}
