package neo.training.designpattern.adapter;

public class Adapter extends Target {

  private Adaptee adaptee = new Adaptee();

  @Override
  public void request() {
    super.request();
    adaptee.foo();
  }

}
