package neo.training.designpattern.adapter;

import neo.training.designpattern.Client;

public abstract class Target {

  public void request() {
    Client.echo("Target request");
  }
}
