package neo.training.designpattern.adapter;

import neo.training.designpattern.Client;

public class AdapterClient {

  public static void main(String[] args) {
    Client.setHeader(Adapter.class);

    Target target = new Adapter();
    target.request();
  }
}
