package neo.training.designpattern.memento;

public class CheckPoint {

  private Memento memento;

  public Memento getMemento() {
    return memento;
  }

  public void setMemento(Memento memento) {
    this.memento = memento;
  }

}
