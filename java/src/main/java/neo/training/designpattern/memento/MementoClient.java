package neo.training.designpattern.memento;

import neo.training.designpattern.Client;

public class MementoClient {

  public static void main(String[] args) {
    Client.setHeader(Memento.class);

    Originator originator = new Originator();
    originator.setState(0);
    Client.echo("state = " + originator.getState());

    CheckPoint checkPoint = new CheckPoint();
    checkPoint.setMemento(originator.createMemento());

    originator.setState(1);
    Client.echo("state = " + originator.getState());

    originator.setMemento(checkPoint.getMemento());
    Client.echo("state = " + originator.getState());
  }
}
