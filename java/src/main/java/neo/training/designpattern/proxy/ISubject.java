package neo.training.designpattern.proxy;

public interface ISubject {

  void foo();
}
