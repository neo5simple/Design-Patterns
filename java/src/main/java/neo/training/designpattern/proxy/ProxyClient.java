package neo.training.designpattern.proxy;

import neo.training.designpattern.Client;

public class ProxyClient {

  public static void main(String[] args) {
    Client.setHeader(Proxy.class);

    Proxy proxy = new Proxy();
    proxy.foo();
  }
}
