package neo.training.designpattern.proxy;

import neo.training.designpattern.Client;

public class RealSubject implements ISubject {

  @Override
  public void foo() {
    Client.echo("Real Subject");
  }
}
