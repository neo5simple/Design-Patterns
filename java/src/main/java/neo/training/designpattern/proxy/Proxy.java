package neo.training.designpattern.proxy;

public class Proxy implements ISubject {

  private RealSubject subject;

  @Override
  public void foo() {
    if (null == subject) {
      subject = new RealSubject();
    }

    subject.foo();
  }
}
