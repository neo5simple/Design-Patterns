package neo.training.designpattern.singleton;

import neo.training.designpattern.Client;

public class SingletonClient {

  public static void main(String[] args) {
    Client.setHeader(Singleton.class);

    Singleton singletonA = Singleton.getInstance();
    Singleton singletonB = Singleton.getInstance();

    if (singletonA.equals(singletonB)) {
      Client.echo("A equals B");
    } else {
      Client.echo("A not equals B");
    }

    if (singletonA == singletonB) {
      Client.echo("A == B");
    } else {
      Client.echo("A != B");
    }
  }
}
