package neo.training.designpattern.component;

import neo.training.designpattern.Client;

public class ComponentClient {

  public static void main(String[] args) {
    Client.setHeader(Component.class);

    Component root = new Composite("root");
    root.add(new Leaf("Leaf A"));
    root.add(new Leaf("Leaf B"));

    Composite compositeA = new Composite("composite A");
    compositeA.add(new Leaf("composite A Leaf A"));
    compositeA.add(new Leaf("composite A Leaf B"));
    root.add(compositeA);

    Composite compositeB = new Composite("composite B");
    compositeB.add(new Leaf("composite B Leaf A"));
    compositeB.add(new Leaf("composite B Leaf B"));
    compositeA.add(compositeB);

    root.display(0);
  }
}
