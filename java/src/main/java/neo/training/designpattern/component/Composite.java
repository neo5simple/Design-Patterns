package neo.training.designpattern.component;

import java.util.ArrayList;
import java.util.List;
import neo.training.designpattern.Client;

public class Composite extends Component {

  private List<Component> childrenComponents = new ArrayList<Component>();

  public Composite(String name) {
    super(name);
  }

  @Override
  public void add(Component component) {
    childrenComponents.add(component);
  }

  @Override
  public void remove(Component component) {
    childrenComponents.remove(component);
  }

  @Override
  public void display(int depth) {
    String dummy = "";
    for (int i = 0; i < depth; i++) {
      dummy += "\t";
    }

    Client.echo(dummy + "[+] " + getName());

    for (Component component : childrenComponents) {
      component.display(depth + 1);
    }
  }

}
