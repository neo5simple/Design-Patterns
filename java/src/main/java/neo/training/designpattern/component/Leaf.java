package neo.training.designpattern.component;

import neo.training.designpattern.Client;

public class Leaf extends Component {

  public Leaf(String name) {
    super(name);
  }

  @Override
  public void add(Component component) {
    Client.error("cannot add into a leaf");
  }

  @Override
  public void remove(Component component) {
    Client.error("cannot remove from a leaf");
  }

  @Override
  public void display(int depth) {
    String dummy = "";
    for (int i = 0; i < depth; i++) {
      dummy += "\t";
    }

    Client.echo(dummy + "[-] " + getName());
  }

}
