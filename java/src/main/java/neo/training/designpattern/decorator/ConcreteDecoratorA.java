package neo.training.designpattern.decorator;

import neo.training.designpattern.Client;

public class ConcreteDecoratorA extends Decorator {

  @Override
  public void foo() {
    super.foo();
    Client.echo("Concrete Decorator A");
  }

}
