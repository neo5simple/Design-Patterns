package neo.training.designpattern.decorator;

import neo.training.designpattern.Client;

public class DecoratorClient {

  public static void main(String[] args) {
    Client.setHeader(Decorator.class);

    ConcreteComponent component = new ConcreteComponent();
    ConcreteDecoratorA decoratorA = new ConcreteDecoratorA();
    ConcreteDecoratorB decoratorB = new ConcreteDecoratorB();

    decoratorA.setComponent(component);
    decoratorB.setComponent(decoratorA);
    decoratorB.foo();
  }
}
