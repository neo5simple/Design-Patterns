package neo.training.designpattern.decorator;

public abstract class Component {

  public abstract void foo();
}
