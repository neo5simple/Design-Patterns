package neo.training.designpattern.decorator;

import neo.training.designpattern.Client;

public class ConcreteDecoratorB extends Decorator {

  @Override
  public void foo() {
    super.foo();
    Client.echo("Concrete Decorator B");
  }

}
