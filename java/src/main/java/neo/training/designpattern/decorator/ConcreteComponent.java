package neo.training.designpattern.decorator;

import neo.training.designpattern.Client;

public class ConcreteComponent extends Component {

  @Override
  public void foo() {
    Client.echo("Default Decorator");
  }
}
