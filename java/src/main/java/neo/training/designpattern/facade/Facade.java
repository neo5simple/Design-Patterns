package neo.training.designpattern.facade;

import neo.training.designpattern.Client;

public class Facade {

  private SubSystemA systemA;
  private SubSystemB systemB;
  private SubSystemC systemC;

  public Facade() {
    systemA = new SubSystemA();
    systemB = new SubSystemB();
    systemC = new SubSystemC();
  }

  public void foo1() {
    Client.echo("foo1");
    systemA.foo();
    systemB.foo();
    systemC.foo();
  }

  public void foo2() {
    Client.echo("foo2");
    systemC.foo();
    systemB.foo();
    systemA.foo();
  }
}
