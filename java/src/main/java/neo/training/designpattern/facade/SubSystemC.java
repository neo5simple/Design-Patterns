package neo.training.designpattern.facade;

import neo.training.designpattern.Client;

public class SubSystemC {

  public void foo() {
    Client.echo("Sub-System C");
  }
}
