package neo.training.designpattern.facade;

import neo.training.designpattern.Client;

public class SubSystemB {

  public void foo() {
    Client.echo("Sub-System B");
  }
}
