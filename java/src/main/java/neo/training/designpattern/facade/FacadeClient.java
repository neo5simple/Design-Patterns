package neo.training.designpattern.facade;

import neo.training.designpattern.Client;

public class FacadeClient {

  public static void main(String[] args) {
    Client.setHeader(Facade.class);

    Facade facade = new Facade();
    facade.foo1();
    facade.foo2();
  }
}
