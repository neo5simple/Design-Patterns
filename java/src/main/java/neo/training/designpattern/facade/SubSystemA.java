package neo.training.designpattern.facade;

import neo.training.designpattern.Client;

public class SubSystemA {

  public void foo() {
    Client.echo("Sub-System A");
  }
}
