package neo.training.designpattern.factory.simple;

public class SimpleFactory {

  public static final int OPERATION_A = 0x01;
  public static final int OPERATION_B = 0x02;

  public static Operation foo(int type) {
    Operation operation = null;
    switch (type) {
      case OPERATION_A:
        operation = new OperationA();
        break;

      case OPERATION_B:
        operation = new OperationB();
        break;

      default:
        break;
    }

    return operation;
  }

}
