package neo.training.designpattern.factory.simple;

import neo.training.designpattern.Client;

public class SimpleFactoryClient {

  public static void main(String[] args) {
    Client.setHeader(SimpleFactory.class);

    SimpleFactory.foo(SimpleFactory.OPERATION_A).foo();
    SimpleFactory.foo(SimpleFactory.OPERATION_B).foo();
  }
}
