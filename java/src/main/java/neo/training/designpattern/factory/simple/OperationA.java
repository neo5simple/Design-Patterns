package neo.training.designpattern.factory.simple;

import neo.training.designpattern.Client;

public class OperationA extends Operation {

  @Override
  public void foo() {
    Client.echo("Operation A");
  }
}
