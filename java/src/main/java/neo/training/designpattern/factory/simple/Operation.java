package neo.training.designpattern.factory.simple;

import neo.training.designpattern.Client;

public abstract class Operation {

  public void foo() {
    Client.echo("Default Operation");
  }
}
