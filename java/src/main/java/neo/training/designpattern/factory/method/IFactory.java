package neo.training.designpattern.factory.method;

public interface IFactory {

  IProduct createProduct();
}
