package neo.training.designpattern.factory.method;

import neo.training.designpattern.Client;

public class FactoryMethodClient {

  public static void main(String[] args) {
    Client.setHeader(IFactory.class);

    IFactory factory;
    IProduct product;

    factory = new ConcreteFactoryA();
    product = factory.createProduct();
    product.foo();

    factory = new ConcreteFactoryB();
    product = factory.createProduct();
    product.foo();
  }
}
