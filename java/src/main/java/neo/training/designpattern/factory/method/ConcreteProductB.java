package neo.training.designpattern.factory.method;

import neo.training.designpattern.Client;

public class ConcreteProductB implements IProduct {

  @Override
  public void foo() {
    Client.echo("Concrete Product B");
  }

}
