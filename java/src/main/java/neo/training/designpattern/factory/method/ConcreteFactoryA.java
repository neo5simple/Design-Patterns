package neo.training.designpattern.factory.method;

public class ConcreteFactoryA implements IFactory {

  @Override
  public IProduct createProduct() {
    return new ConcreteProductA();
  }

}
