package neo.training.designpattern.factory.simple;

import neo.training.designpattern.Client;

public class OperationB extends Operation {

  @Override
  public void foo() {
    Client.echo("Operation B");
  }
}
