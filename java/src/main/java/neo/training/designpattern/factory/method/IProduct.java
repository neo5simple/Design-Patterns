package neo.training.designpattern.factory.method;

public interface IProduct {

  void foo();
}
