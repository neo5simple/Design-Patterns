package neo.training.designpattern.factory.method;

public class ConcreteFactoryB implements IFactory {

  @Override
  public IProduct createProduct() {
    return new ConcreteProductB();
  }

}
