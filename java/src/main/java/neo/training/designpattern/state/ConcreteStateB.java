package neo.training.designpattern.state;

import neo.training.designpattern.Client;

public class ConcreteStateB implements IState {

  private Context context;

  public ConcreteStateB(Context context) {
    this.context = context;
  }

  @Override
  public void foo() {
    Client.echo("Concrete State B, could change to C");
  }

  @Override
  public void change() {
    context.setState(new ConcreteStateC(context));
  }

}
