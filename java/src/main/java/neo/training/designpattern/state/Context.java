package neo.training.designpattern.state;

public class Context {

  private IState state;

  public Context() {
    state = new ConcreteStateA(this);
  }

  public void setState(IState state) {
    this.state = state;
  }

  public void foo() {
    state.foo();
    state.change();
  }

}
