package neo.training.designpattern.state;

import neo.training.designpattern.Client;

public class StateClient {

  public static void main(String[] args) {
    Client.setHeader(IState.class);

    Context context = new Context();
    context.foo();
    context.foo();
    context.foo();
    context.foo();
  }
}
