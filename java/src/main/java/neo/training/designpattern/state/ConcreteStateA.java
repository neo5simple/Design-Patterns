package neo.training.designpattern.state;

import neo.training.designpattern.Client;

public class ConcreteStateA implements IState {

  private Context context;

  public ConcreteStateA(Context context) {
    this.context = context;
  }

  @Override
  public void foo() {
    Client.echo("Concrete State A, could change to B");
  }

  @Override
  public void change() {
    context.setState(new ConcreteStateB(context));
  }

}
