package neo.training.designpattern.state;

public interface IState {

  void foo();

  void change();
}
