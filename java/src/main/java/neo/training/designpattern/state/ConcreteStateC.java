package neo.training.designpattern.state;

import neo.training.designpattern.Client;

public class ConcreteStateC implements IState {

  private Context context;

  public ConcreteStateC(Context context) {
    this.context = context;
  }

  @Override
  public void foo() {
    Client.echo("Concrete State C, could change to B");
  }

  @Override
  public void change() {
    context.setState(new ConcreteStateB(context));
  }

}
