package neo.training.designpattern.publish_subscribe;

public interface IObserver {

  void update();
}
