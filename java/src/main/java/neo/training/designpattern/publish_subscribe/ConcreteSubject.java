package neo.training.designpattern.publish_subscribe;

public class ConcreteSubject extends Subject {

  private int subjectState;

  public int getSubjectState() {
    return subjectState;
  }

  public void setSubjectState(int subjectState) {
    this.subjectState = subjectState;
  }

}
