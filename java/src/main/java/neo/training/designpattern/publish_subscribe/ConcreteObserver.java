package neo.training.designpattern.publish_subscribe;

import neo.training.designpattern.Client;

public class ConcreteObserver implements IObserver {

  private int observerState;
  private ConcreteSubject subject;

  public ConcreteObserver(ConcreteSubject subject) {
    observerState = 0;
    this.subject = subject;
  }

  @Override
  public void update() {
    int lastState = observerState;
    observerState = subject.getSubjectState();
    Client.echo(
        "(" + hashCode() + ") update: " + lastState + "->" + observerState);
  }

  public ConcreteSubject getSubject() {
    return subject;
  }

  public void setSubject(ConcreteSubject subject) {
    this.subject = subject;
  }

}
