package neo.training.designpattern.publish_subscribe;

import neo.training.designpattern.Client;

public class PublishSubscribeClient {

  public static void main(String[] args) {
    Client.setHeader(IObserver.class);

    ConcreteSubject subject = new ConcreteSubject();
    subject.add(new ConcreteObserver(subject));
    subject.add(new ConcreteObserver(subject));

    subject.setSubjectState(1);
    subject.onNotify();
  }
}
