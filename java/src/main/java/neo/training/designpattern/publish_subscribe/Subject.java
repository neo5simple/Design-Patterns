package neo.training.designpattern.publish_subscribe;

import java.util.ArrayList;
import neo.training.designpattern.Client;

public abstract class Subject {

  private ArrayList<IObserver> observers = new ArrayList();

  public void add(IObserver observer) {
    observers.add(observer);
  }

  public void remove(IObserver observer) {
    observers.remove(observers);
  }

  public void onNotify() {
    for (IObserver observer : observers) {
      observer.update();
    }
  }
}
