package neo.training.designpattern.strategy;

import neo.training.designpattern.Client;

public class StrategyA implements IStrategy {

  @Override
  public void foo() {
    Client.echo("Strategy A");
  }
}
