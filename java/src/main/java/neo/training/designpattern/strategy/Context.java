package neo.training.designpattern.strategy;

public class Context {

  private IStrategy strategy = null;

  public Context(IStrategy strategy) {
    this.strategy = strategy;
  }

  public void foo() {
    strategy.foo();
  }
}
