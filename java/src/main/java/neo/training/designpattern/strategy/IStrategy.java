package neo.training.designpattern.strategy;

public interface IStrategy {

  void foo();
}
