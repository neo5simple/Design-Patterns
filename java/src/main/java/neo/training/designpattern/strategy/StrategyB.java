package neo.training.designpattern.strategy;

import neo.training.designpattern.Client;

public class StrategyB implements IStrategy {

  @Override
  public void foo() {
    Client.echo("Strategy B");
  }
}
