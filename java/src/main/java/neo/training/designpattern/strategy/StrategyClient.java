package neo.training.designpattern.strategy;

import neo.training.designpattern.Client;

public class StrategyClient {

  public static void main(String[] args) {
    Client.setHeader(IStrategy.class);

    Context context;

    context = new Context(new StrategyA());
    context.foo();

    context = new Context(new StrategyB());
    context.foo();
  }
}
