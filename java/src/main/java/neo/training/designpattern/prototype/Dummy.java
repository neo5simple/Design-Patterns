package neo.training.designpattern.prototype;

public class Dummy implements Cloneable {

  public String dummyString = "Dummy";

  @Override
  protected Object clone() throws CloneNotSupportedException {
    return super.clone();
  }

}
