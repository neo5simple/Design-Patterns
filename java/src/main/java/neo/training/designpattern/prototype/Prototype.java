package neo.training.designpattern.prototype;

import java.io.Serializable;

public abstract class Prototype implements Cloneable, Serializable {

  private static final long serialVersionUID = 7898573883141942222L;

  public Dummy dummy;
  public String dummyString;

  public Prototype() {
    dummy = new Dummy();
    dummyString = "default";

//    Client.echo(" >>> original");
//    Client.echo(
//        " <<< dummyString = " + dummyString + ", " + dummyString.hashCode());
//    Client.echo(" <<< dummy = " + dummy.toString() + ", " + dummy.hashCode());
//    Client.echo(" <<< dummy.dummyString = " + dummy.dummyString + ", "
//        + dummy.dummyString.hashCode());
  }

  public Prototype deepClone() {
    Prototype prototype = null;

    try {
//      ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
//      new ObjectOutputStream(outputStream).writeObject(this);
//
//      ByteArrayInputStream inputStream = new ByteArrayInputStream(
//          outputStream.toByteArray());
//      prototype = (Prototype) (new ObjectInputStream(inputStream)
//          .readObject());

//      Client.echo(" >>> Object Stream");
//      Client.echo(" <<< dummyString = " + prototype.dummyString + ", "
//          + prototype.dummyString.hashCode());
//      Client.echo(
//          " <<< dummy = " + prototype.dummy.toString() + ", " + prototype.dummy
//              .hashCode());
//      Client.echo(
//          " <<< dummy.dummyString = " + prototype.dummy.dummyString + ", " +
//              prototype.dummy.dummyString.hashCode());
    } catch (Exception e) {
      e.printStackTrace();
    }

    try {
      prototype = (Prototype) super.clone();
      prototype.dummy = (Dummy) dummy.clone();

//      Client.echo(" >>> clone() x2");
//      Client.echo(" <<< dummyString = " + prototype.dummyString + ", "
//          + prototype.dummyString.hashCode());
//      Client.echo(
//          " <<< dummy = " + prototype.dummy.toString() + ", " + prototype.dummy
//              .hashCode());
//      Client.echo(
//          " <<< dummy.dummyString = " + prototype.dummy.dummyString + ", " +
//              prototype.dummy.dummyString.hashCode());
    } catch (Exception e) {
      e.printStackTrace();
    }

    return prototype;
  }

  @Override
  protected Object clone() {
    Object object = null;
    try {
      object = super.clone();
//      object = deepClone();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return object;
  }

}
