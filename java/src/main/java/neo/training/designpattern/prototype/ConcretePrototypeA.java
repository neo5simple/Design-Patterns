package neo.training.designpattern.prototype;

import neo.training.designpattern.Client;

public class ConcretePrototypeA extends Prototype {

  private static final long serialVersionUID = -5788759589879848027L;

  public Prototype deepClone() {
    Client.echo("Concrete Prototype A deepClone");
    return super.deepClone();
  }
}
