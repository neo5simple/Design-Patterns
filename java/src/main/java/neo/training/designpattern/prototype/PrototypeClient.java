package neo.training.designpattern.prototype;

import neo.training.designpattern.Client;

public class PrototypeClient {

  public static void main(String[] args) {
    Client.setHeader(Prototype.class);

    ConcretePrototypeA prototypeA = new ConcretePrototypeA();
    ConcretePrototypeA prototypeA2 = (ConcretePrototypeA) prototypeA
        .deepClone();

    prototypeA.dummyString = "a";
    prototypeA.dummy.dummyString = "a.dummy";
    Client.echo(prototypeA.dummyString + ", " + prototypeA.dummy.dummyString);

    prototypeA2.dummyString = "a2";
    prototypeA2.dummy.dummyString = "a2.dummy";
    Client.echo(prototypeA.dummyString + ", " + prototypeA.dummy.dummyString);

    ConcretePrototypeB prototypeB = new ConcretePrototypeB();
    ConcretePrototypeB prototypeB2 = (ConcretePrototypeB) prototypeB.clone();

    prototypeB.dummyString = "b";
    prototypeB.dummy.dummyString = "b.dummy";
    Client.echo(prototypeB.dummyString + ", " + prototypeB.dummy.dummyString);

    prototypeB2.dummyString = "b2";
    prototypeB2.dummy.dummyString = "b2.dummy";
    Client.echo(prototypeB.dummyString + ", " + prototypeB.dummy.dummyString);
  }
}
