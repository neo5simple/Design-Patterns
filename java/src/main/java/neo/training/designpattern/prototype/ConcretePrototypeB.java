package neo.training.designpattern.prototype;

import neo.training.designpattern.Client;

public class ConcretePrototypeB extends Prototype {

  private static final long serialVersionUID = 2390811705169131960L;

  @Override
  public Prototype deepClone() {
    Client.echo("Concrete Prototype B deepClone");
    return super.deepClone();
  }

}
