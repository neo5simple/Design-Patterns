package neo.training.designpattern.builder;

public class ConcreteBuilderB implements IBuilder {

  private Product product = new Product();

  @Override
  public void buildPartA() {
    product.add("Builder B Part A");
  }

  @Override
  public void buildPartB() {
    product.add("Builder B Part B");
  }

  @Override
  public Product getProduct() {
    return product;
  }

}
