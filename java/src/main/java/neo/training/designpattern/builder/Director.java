package neo.training.designpattern.builder;

public class Director {

  public void construct(IBuilder builder) {
    builder.buildPartA();
    builder.buildPartB();
  }
}
