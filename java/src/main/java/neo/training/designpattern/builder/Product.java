package neo.training.designpattern.builder;

import java.util.ArrayList;
import java.util.List;
import neo.training.designpattern.Client;

public class Product {

  private ArrayList parts = new ArrayList();

  public void add(String part) {
    parts.add(part);
  }

  public void foo() {
    for (int i = 0; i < parts.size(); i++) {
      Client.echo("parts[" + i + "]: " + parts.get(i));
    }
  }
}
