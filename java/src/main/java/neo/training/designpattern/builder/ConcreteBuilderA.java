package neo.training.designpattern.builder;

public class ConcreteBuilderA implements IBuilder {

  private Product product = new Product();

  @Override
  public void buildPartA() {
    product.add("Builder A Part A");
  }

  @Override
  public void buildPartB() {
    product.add("Builder A Part B");
  }

  @Override
  public Product getProduct() {
    return product;
  }

}
