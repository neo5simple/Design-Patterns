package neo.training.designpattern.builder;

import neo.training.designpattern.Client;

public class BuilderClient {

  public static void main(String[] args) {
    Client.setHeader(IBuilder.class);

    Director director = new Director();
    IBuilder builderA = new ConcreteBuilderA();
    IBuilder builderB = new ConcreteBuilderB();

    director.construct(builderA);
    Product product = builderA.getProduct();
    product.foo();

    director.construct(builderB);
    product = builderB.getProduct();
    product.foo();
  }
}
