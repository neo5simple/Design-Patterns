package neo.training.designpattern.builder;

public interface IBuilder {

  void buildPartA();

  void buildPartB();

  Product getProduct();
}
