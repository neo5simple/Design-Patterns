package neo.training.designpattern;


import neo.training.designpattern.adapter.AdapterClient;
import neo.training.designpattern.builder.BuilderClient;
import neo.training.designpattern.component.ComponentClient;
import neo.training.designpattern.decorator.DecoratorClient;
import neo.training.designpattern.facade.FacadeClient;
import neo.training.designpattern.factory.method.FactoryMethodClient;
import neo.training.designpattern.factory.simple.SimpleFactoryClient;
import neo.training.designpattern.iterator.IteratorClient;
import neo.training.designpattern.memento.MementoClient;
import neo.training.designpattern.prototype.PrototypeClient;
import neo.training.designpattern.proxy.ProxyClient;
import neo.training.designpattern.publish_subscribe.PublishSubscribeClient;
import neo.training.designpattern.singleton.SingletonClient;
import neo.training.designpattern.state.StateClient;
import neo.training.designpattern.strategy.StrategyClient;
import neo.training.designpattern.templateMethod.TemplateMethodClient;
import neo.utils.Log;

public class Client {

  private static Client instance;
  private static String header = "";

  private Client() {
  }

  public static Client getInstance() {
    if (null == instance) {
      instance = new Client();
      Log.init();
    }

    return instance;
  }

  public static void setHeader(Class clz) {
    getInstance();
    Log.e("******");

    header = (null != clz) ? clz.getSimpleName() : "-";
  }

  public static void echo(String msg) {
    Log.d(header + ": " + msg);
  }

  public static void error(String msg) {
    Log.e(header + ": " + msg);
  }

  public static void main(String[] args) {
    DecoratorClient.main(null);
    StateClient.main(null);
    StrategyClient.main(null);
    ProxyClient.main(null);
    SimpleFactoryClient.main(null);
    FactoryMethodClient.main(null);
    PrototypeClient.main(null);
    TemplateMethodClient.main(null);
    FacadeClient.main(null);
    BuilderClient.main(null);
    PublishSubscribeClient.main(null);
    AdapterClient.main(null);
    MementoClient.main(null);
    ComponentClient.main(null);
    IteratorClient.main(null);
    SingletonClient.main(null);
  }
}
