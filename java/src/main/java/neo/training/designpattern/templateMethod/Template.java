package neo.training.designpattern.templateMethod;

public abstract class Template {

  public abstract void foo1();

  public abstract void foo2();

  public void foo() {
    foo1();
    foo2();
  }
}
