package neo.training.designpattern.templateMethod;

import neo.training.designpattern.Client;

public class ConcreteClassA extends Template {

  @Override
  public void foo1() {
    Client.echo("Concrete Class A foo1");
  }

  @Override
  public void foo2() {
    Client.echo("Concrete Class A foo2");
  }

}
