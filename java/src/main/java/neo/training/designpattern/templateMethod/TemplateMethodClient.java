package neo.training.designpattern.templateMethod;

import neo.training.designpattern.Client;

public class TemplateMethodClient {

  public static void main(String[] args) {
    Client.setHeader(Template.class);

    Template template;

    template = new ConcreteClassA();
    template.foo();

    template = new ConcreteClassB();
    template.foo();
  }
}
