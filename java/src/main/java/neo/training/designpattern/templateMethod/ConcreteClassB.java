package neo.training.designpattern.templateMethod;

import neo.training.designpattern.Client;

public class ConcreteClassB extends Template {

  @Override
  public void foo1() {
    Client.echo("Concrete Class B foo1");
  }

  @Override
  public void foo2() {
    Client.echo("Concrete Class B foo2");
  }

}
