package neo.training.designpattern.iterator;

public interface Iterator {

  Object getFirst();

  Object getNext();

  Object getCurrent();

  boolean isDone();
}
