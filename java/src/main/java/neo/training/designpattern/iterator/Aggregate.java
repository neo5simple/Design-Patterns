package neo.training.designpattern.iterator;

public interface Aggregate {

  Iterator createIterator();
}
