package neo.training.designpattern.iterator;

import java.util.ArrayList;

public class ConcreteAggregate implements Aggregate {

  private ArrayList<Object> items = new ArrayList();

  @Override
  public Iterator createIterator() {
    return new ConcreteIterator(this);
  }

  public boolean add(Object object) {
    return items.add(object);
  }

  public int size() {
    return items.size();
  }

  public Object get(int index) {
    return items.get(index);
  }

}
