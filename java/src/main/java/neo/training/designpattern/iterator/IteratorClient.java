package neo.training.designpattern.iterator;

import neo.training.designpattern.Client;

public class IteratorClient {

  public static void main(String[] args) {
    Client.setHeader(Iterator.class);

    ConcreteAggregate aggregate = new ConcreteAggregate();
    aggregate.add("jude");
    aggregate.add("oops!");

    Iterator iterator = new ConcreteIterator(aggregate);
    iterator.getFirst();
    while (false == iterator.isDone()) {
      Client.echo(iterator.getCurrent().toString());
      iterator.getNext();
    }
  }
}
