package neo.training.designpattern.iterator;

public class ConcreteIterator implements Iterator {

  private int index;
  private ConcreteAggregate aggregate;

  public ConcreteIterator(ConcreteAggregate aggregate) {
    index = 0;
    this.aggregate = aggregate;
  }

  @Override
  public Object getFirst() {
    index = 0;
    return aggregate.get(index);
  }

  @Override
  public Object getNext() {
    Object object = null;
    index++;
    if (index < aggregate.size()) {
      object = aggregate.get(index);
    }
    return object;
  }

  @Override
  public Object getCurrent() {
    return aggregate.get(index);
  }

  @Override
  public boolean isDone() {
    return (index >= aggregate.size());
  }

}
